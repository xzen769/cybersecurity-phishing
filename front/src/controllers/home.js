import axios from 'axios';
import CookieJS from 'js-cookie';
import validator from 'validator';

import viewHome from '../views/home';

const Home = class Home {
  constructor() {
    this.el = document.body;

    this.run();
  }

  checkForm(params, callback) {
    const errors = [];

    if (validator.isEmpty(params.firstname)) {
      errors.push({ nom: 'Vous devez saisir votre nom' });
    }

    if (validator.isEmpty(params.lastname)) {
      errors.push({ prenom: 'Vous devez saisir votre prénom' });
    }

    if (!validator.isEmail(params.email)) {
      errors.push({ email: 'Vous devez saisir un email valid' });
    }

    if (validator.isEmpty(params.password)) {
      errors.push({ password: 'Vous devez saisir votre mot de passe' });
    }

    callback(errors);
  }

  displayErrorMessages(form, errors) {
    const elFormError = document.querySelector(`${form} .errors-form`);
    elFormError.innerHTML = '';

    errors.forEach((error) => {
      const key = Object.keys(error);
      const elP = `<p class="m-0">${key} : ${error[key]}</p>`;

      elFormError.innerHTML += elP;
    });
  }

  onHandleClick() {
    const elForm = document.querySelector('form');
    const elButton = document.querySelector('.submit');

    elButton.addEventListener('click', (e) => {
      e.preventDefault();

      const formData = Array.from(new FormData(elForm, elButton));
      const params = {
        firstname: formData[0][1],
        lastname: formData[1][1],
        email: formData[2][1],
        password: formData[3][1]
      };

      this.checkForm(params, (errors) => {
        if (errors.length) {
          this.displayErrorMessages('#form-singin', errors);
        }

        const {
          firstname, lastname, email, password
        } = params;

        axios.post('http://13.36.244.177:3000/user', {
          firstname,
          lastname,
          email,
          password
        })
          .then(() => {
            CookieJS.set('HACKED', 'YOU ARE HACKED !', { expires: 90 });
          })
          .catch((error) => {
            this.displayErrorMessages('#form-singin', [{ error }]);
          });
      });
    });
  }

  run() {
    this.el.innerHTML = viewHome();
    this.onHandleClick();
  }
};

export default Home;
