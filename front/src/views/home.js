import logo from '../static/img/logo.png';
// import astee from '../static/img/astee.jpeg';
import scan from '../static/img/scann.png';

export default () => (`
  <header>
    <nav class="navbar navbar-expand-lg bg-ligth shadow-lg p-4">
      <div class="container-fluid">
        <img width="200" src="${logo}" alt="logo">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </nav>
  </header>
  <main class="container">
    <div class="row mt-5">
      <div class="col-10">
        <h1 class="display-5">102e congrès - Conférence Cybersécurité</h1>
        <hr>
        <h2 class="h4">Accés : <strong>7 juin 2023</strong> - <strong>14h45</strong></h2>
      </div>
      <div class="col-2">
        <img class="img-fluid" src="${scan}" alt="logo">
      </div>
      <div class="col-6">
        <div class="row">
          <div class="row">
            <div class="col-12">
              <p class="h4">Merci de vous créer un compte pour obtenir vos identifiants d'accès à la salle de conférence.
              Un QRcode sera généré à la suite de votre inscription qui vous permettra d'accéder à la salle.</p>
              <p class="h5 pt-4">Pour toutes questions relative à cette inscription merci de contacter <strong><a href="https://www.linkedin.com/in/julia-serre-pon%C3%A7on-312541142/">Julia Serre-Ponçon</a></strong></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-6">
        <div class="row mb-2">
          <div class="col-12">
            <p class="h5">Merci d'entrer vos informations :</p>
          </div>
        </div>
        <div class="row">
          <form id="form-singin" class="row g-3">
            <div class="col-md-6">
              <label for="inputfirstname" class="form-label h5">Nom</label>
              <input type="text" class="form-control" id="inputfirstname" name="firstname">
            </div>
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label h5">Prénom</label>
              <input type="text" class="form-control" id="inputlastname" name="lastname">
            </div>
            <div class="col-md-6">
              <label for="inputEmail4" class="form-label h5">Email</label>
              <input type="email" class="form-control" id="inputEmail4" name="email">
            </div>
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label h5">Password</label>
              <input type="password" class="form-control" id="inputPassword4" name="password">
            </div>
            <div class="col-12 d-grid mx-auto">
              <button type="submit" class="submit btn btn-dark">Générer le QR Code</button>
            </div>
            <div class="errors-form text-danger pt-2"></div>
          </form>
        </div>
      </div>
    </div>
  </main>
`);
